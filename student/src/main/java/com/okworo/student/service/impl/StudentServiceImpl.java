package com.okworo.student.service.impl;

import com.okworo.student.model.Student;
import com.okworo.student.service.StudentService;

import java.util.List;

/**
 * @author Morris.Okworo on 17/06/2024
 */

public class StudentServiceImpl implements StudentService {
    @Override
    public List<Student> findAll() {
        return List.of(new Student(1L, "Morris", "Okworo", 30),
                new Student(2L, "John", "Doe", 50));
    }
}
