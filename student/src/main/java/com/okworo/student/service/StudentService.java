package com.okworo.student.service;

import com.okworo.student.model.Student;
import com.okworo.student.service.impl.StudentServiceImpl;

import java.util.List;

/**
 * @author Morris.Okworo on 17/06/2024
 */

public interface StudentService {
    List<Student> findAll();


    static StudentService build() {

        return new StudentServiceImpl();
    }

}
