package com.okworo.springmodulesdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmodulesdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringmodulesdemoApplication.class, args);
    }

}
