package com.okworo.springmodulesdemo;

import com.okworo.student.model.Student;
import com.okworo.student.service.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Morris.Okworo on 17/06/2024
 */
@RestController
public class controller {

    @GetMapping("hello")
    public String hello() {
        return "Hello";
    }

    @GetMapping("students")
    public List<Student> findAllStudents() {
        StudentService studentService = StudentService.build();
        return studentService.findAll();
    }
}
